package com.app.currencymvvm

object ConstantsApp {

    const val BASE_URL = "https://jsonplaceholder.typicode.com/"

    object Arguments {
        const val ARG_ALBUM_TO_PHOTO_FRAGMENT = "ARG_ALBUM_TO_PHOTO_FRAGMENT"
        const val ARG_PHOTO_TO_PHOTO_DETAIL_FRAGMENT = "ARG_PHOTO_TO_PHOTO_DETAIL_FRAGMENT"
    }
}