package com.app.currencymvvm.api

import javax.inject.Inject

class ApiClient @Inject constructor(
    private val apiInterface: ApiInterface
) {
    fun getAlbums() = apiInterface.getAlbums()

    fun getPhotosByAlbumId(albumId: Long) = apiInterface.getPhotos(albumId)

    fun getPhotoDetailById(photoId: Long) = apiInterface.getPhotoDetail(photoId)
}