package com.app.currencymvvm.api

import com.app.currencymvvm.model.nw.NwAlbum
import com.app.currencymvvm.model.nw.NwPhoto
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiInterface {

    @GET("albums/")
    fun getAlbums(): Single<List<NwAlbum>>

    @GET("albums/{id}/photos")
    fun getPhotos(
        @Path("id") id: Long
    ): Single<List<NwPhoto>>

    @GET("photos/{id}")
    fun getPhotoDetail(
        @Path("id") id: Long
    ):Single<NwPhoto>
}