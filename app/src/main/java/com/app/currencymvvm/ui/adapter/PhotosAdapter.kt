package com.app.currencymvvm.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.app.currencymvvm.R
import com.app.currencymvvm.databinding.ItemPhotoBinding
import com.app.currencymvvm.model.db.Photo
import com.app.currencymvvm.view_model.PhotoViewModel
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import timber.log.Timber
import java.lang.Exception

class PhotosAdapter(val onPhotoClickListener: OnPhotoClickListener): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val photos = mutableListOf<Photo>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val viewDataBinding = DataBindingUtil.inflate<ViewDataBinding>(
            LayoutInflater.from(parent.context), R.layout.item_photo, parent, false
        )

        return PhotoViewHolder(viewDataBinding)
    }

    override fun getItemCount() = photos.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as PhotoViewHolder).onBind(getItem(position))
    }

    private fun getItem(position: Int): Photo{
        return photos[position]
    }

    fun addData(photoList: List<Photo>){
        this.photos.clear()
        this.photos.addAll(photoList)
        notifyDataSetChanged()
    }

    inner class PhotoViewHolder(private val dataBinding: ViewDataBinding): RecyclerView.ViewHolder(dataBinding.root){

        fun onBind(photo: Photo){
            val holderPhotoBinding = dataBinding as ItemPhotoBinding
            holderPhotoBinding.photoViewModel = PhotoViewModel(photo)
            holderPhotoBinding.photoProgressBar.visibility = View.VISIBLE

            Picasso.get().load(photo.url).into(holderPhotoBinding.photoImageView, object : Callback{
                override fun onSuccess() {
                    holderPhotoBinding.photoProgressBar.visibility = View.GONE
                }

                override fun onError(e: Exception?) {
                    Timber.e(e)
                    e?.printStackTrace()
                }

            })

            itemView.setOnClickListener {
                onPhotoClickListener.onPhotoClicked(photo)
            }
        }

    }
}