package com.app.currencymvvm.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.app.currencymvvm.ConstantsApp
import com.app.currencymvvm.R
import com.app.currencymvvm.databinding.FragmentPhotoDetailBinding
import com.app.currencymvvm.view_model.PhotoDetailViewModel
import com.app.currencymvvm.view_model.ViewModelFactory
import com.squareup.picasso.Picasso
import dagger.android.support.DaggerFragment
import timber.log.Timber
import javax.inject.Inject

class PhotoDetailFragment: DaggerFragment() {

    var photoId: Long? = null
    private lateinit var fragmentPhotoDetailBinding: FragmentPhotoDetailBinding

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private val photoDetailViewModel: PhotoDetailViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(PhotoDetailViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        photoId = arguments?.getLong(ConstantsApp.Arguments.ARG_PHOTO_TO_PHOTO_DETAIL_FRAGMENT)
        photoDetailViewModel.loadPhotoFromDb(photoId!!)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        fragmentPhotoDetailBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_photo_detail, container, false)
        fragmentPhotoDetailBinding.photoDetailViewModel = photoDetailViewModel

        photoDetailViewModel.isLoad.observe(this, Observer {
            fragmentPhotoDetailBinding.photoDetailProgressBar.visibility = if (it) View.GONE else View.VISIBLE
        })

        photoDetailViewModel.photoReceivedLiveData.observe(this, Observer {
            fragmentPhotoDetailBinding.photoTitle.text = it.title
            Picasso.get().load(it.url).into(fragmentPhotoDetailBinding.photoDetailImageView)
        })

        return fragmentPhotoDetailBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Timber.d("ARG: $photoId")
    }
}