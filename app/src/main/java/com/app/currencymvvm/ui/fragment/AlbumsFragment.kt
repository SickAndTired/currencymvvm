package com.app.currencymvvm.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import com.app.currencymvvm.ConstantsApp
import com.app.currencymvvm.R
import com.app.currencymvvm.databinding.FragmentAlbumsBinding
import com.app.currencymvvm.model.db.Album
import com.app.currencymvvm.ui.adapter.AlbumsAdapter
import com.app.currencymvvm.ui.adapter.OnAlbumClickListener
import com.app.currencymvvm.view_model.AlbumsViewModel
import dagger.android.support.DaggerFragment
import timber.log.Timber
import javax.inject.Inject

class AlbumsFragment: DaggerFragment(), OnAlbumClickListener {

    private lateinit var fragmentAlbumsBinding: FragmentAlbumsBinding
    private var adapter: AlbumsAdapter? = null

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: AlbumsViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(AlbumsViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        adapter = AlbumsAdapter(this)
        viewModel.loadAlbumsFromDb()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        fragmentAlbumsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_albums, container, false)
        fragmentAlbumsBinding.albumsViewModel = viewModel
        fragmentAlbumsBinding.recyclerViewAlbums.adapter = adapter

        viewModel.isLoad.observe(this, Observer {
                visibility -> fragmentAlbumsBinding.progressBarAlbums.visibility = if (visibility) View.GONE else View.VISIBLE
        })

        viewModel.albumsReceivedLiveData.observe(this, Observer {
            initRecyclerView(it)
        })

       return fragmentAlbumsBinding.root
    }

    private fun initRecyclerView(albums: List<Album>) {
        adapter?.addData(albums)
    }

    override fun onAlbumClicked(album: Album) {
        Timber.d("Clicked : $album")
        val bundle = bundleOf(ConstantsApp.Arguments.ARG_ALBUM_TO_PHOTO_FRAGMENT to album.id)
        view!!.findNavController().navigate(R.id.action_albumsFragment_to_photosFragment, bundle)
    }

}