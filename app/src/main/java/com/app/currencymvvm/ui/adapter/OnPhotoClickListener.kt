package com.app.currencymvvm.ui.adapter

import com.app.currencymvvm.model.db.Photo

interface OnPhotoClickListener {
    fun onPhotoClicked(photo: Photo)
}