package com.app.currencymvvm.ui.adapter

import com.app.currencymvvm.model.db.Album

interface OnAlbumClickListener {

    fun onAlbumClicked(album: Album)
}