package com.app.currencymvvm.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import com.app.currencymvvm.ConstantsApp
import com.app.currencymvvm.R
import com.app.currencymvvm.databinding.FragmentPhotosBinding
import com.app.currencymvvm.model.db.Photo
import com.app.currencymvvm.ui.adapter.OnPhotoClickListener
import com.app.currencymvvm.ui.adapter.PhotosAdapter
import com.app.currencymvvm.view_model.PhotosViewModel
import dagger.android.support.DaggerFragment
import timber.log.Timber
import javax.inject.Inject

class PhotosFragment: DaggerFragment(), OnPhotoClickListener {

    var albumId: Long? = null
    private lateinit var fragmentPhotosBinding: FragmentPhotosBinding
    private var adapter: PhotosAdapter? = null

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: PhotosViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(PhotosViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        albumId = arguments?.getLong(ConstantsApp.Arguments.ARG_ALBUM_TO_PHOTO_FRAGMENT)

        adapter = PhotosAdapter(this)
        viewModel.loadPhotosFromDb(albumId!!)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Timber.d("ARG: $albumId")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        fragmentPhotosBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_photos, container, false)
        fragmentPhotosBinding.photosViewModel = viewModel
        fragmentPhotosBinding.recyclerViewPhotos.adapter = adapter

        viewModel.isLoad.observe(this, Observer {
            fragmentPhotosBinding.progressBarPhotos.visibility = if (it) View.GONE else View.VISIBLE
        })

        viewModel.photosReceivedLiveData.observe(this, Observer {
            initRecyclerView(it)
        })

        return fragmentPhotosBinding.root

    }

    private fun initRecyclerView(photoList: List<Photo>){
        adapter?.addData(photoList)
    }

    override fun onPhotoClicked(photo: Photo) {
        Timber.d("On Photo Clicked: $photo")
        val bundle = bundleOf(ConstantsApp.Arguments.ARG_PHOTO_TO_PHOTO_DETAIL_FRAGMENT to photo.id)
        view!!.findNavController().navigate(R.id.action_photosFragment_to_photoDetailFragment, bundle)

    }
}