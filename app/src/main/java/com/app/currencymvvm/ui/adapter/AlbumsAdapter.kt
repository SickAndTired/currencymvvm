package com.app.currencymvvm.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.app.currencymvvm.R
import com.app.currencymvvm.databinding.ItemAlbumBinding
import com.app.currencymvvm.model.db.Album
import com.app.currencymvvm.view_model.AlbumViewModel

class AlbumsAdapter(val onAlbumClickListener: OnAlbumClickListener): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val albums = mutableListOf<Album>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val itemAlbumBinding = DataBindingUtil.inflate<ViewDataBinding>(
            LayoutInflater.from(parent.context), R.layout.item_album, parent, false
        )
        return AlbumViewHolder(itemAlbumBinding)
    }

    override fun getItemCount() = albums.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int){
        (holder as AlbumViewHolder).onBind(getItem(position))
    }

    private fun getItem(position: Int): Album {
        return albums[position]
    }

    fun addData(list: List<Album>) {
        this.albums.clear()
        this.albums.addAll(list)
        notifyDataSetChanged()
    }

    inner class AlbumViewHolder(private val dataBinding: ViewDataBinding) : RecyclerView.ViewHolder(dataBinding.root) {

        fun onBind(album: Album) {
            val holderAlbumBinding = dataBinding as ItemAlbumBinding
            val albumViewModel = AlbumViewModel(album)
            holderAlbumBinding.albumViewModel = albumViewModel

            itemView.setOnClickListener {
                onAlbumClickListener.onAlbumClicked(album)
            }

        }
    }
}