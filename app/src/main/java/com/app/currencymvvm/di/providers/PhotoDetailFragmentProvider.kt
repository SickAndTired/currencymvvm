package com.app.currencymvvm.di.providers

import com.app.currencymvvm.ui.fragment.PhotoDetailFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class PhotoDetailFragmentProvider {

    @ContributesAndroidInjector
    abstract fun providePhotoDetailFragment(): PhotoDetailFragment
}