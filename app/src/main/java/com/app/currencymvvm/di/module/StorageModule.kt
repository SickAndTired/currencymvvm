package com.app.currencymvvm.di.module

import android.app.Application
import androidx.room.Room
import com.app.currencymvvm.db.AppDatabase
import com.app.currencymvvm.db.dao.AlbumDao
import com.app.currencymvvm.db.dao.PhotoDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class StorageModule {

    @Provides
    @Singleton
    internal fun provideDatabase(application: Application) = Room.databaseBuilder(
        application,
        AppDatabase::class.java,
        "database"
    )
        .fallbackToDestructiveMigration()
        .build()

    @Provides
    internal fun provideAlbumDao(appDatabase: AppDatabase): AlbumDao {
        return appDatabase.albumDao()
    }

    @Provides
    internal fun providePhotoDao(appDatabase: AppDatabase): PhotoDao {
        return appDatabase.photoDao()
    }
}