package com.app.currencymvvm.di.module

import com.app.currencymvvm.ConstantsApp
import com.app.currencymvvm.api.ApiInterface
import com.github.leonardoxh.livedatacalladapter.LiveDataCallAdapterFactory
import com.github.leonardoxh.livedatacalladapter.LiveDataResponseBodyConverterFactory
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import timber.log.Timber

@Module
class NetworkModule {

    @Provides
    fun provideOkhttp(interceptor: Interceptor) = OkHttpClient.Builder()
        .addInterceptor(
            HttpLoggingInterceptor(object : HttpLoggingInterceptor.Logger{
                override fun log(message: String) {
                    Timber.tag("OkHttp").d(message)
                }

            })
                .setLevel(HttpLoggingInterceptor.Level.BODY)
        )
        .addInterceptor(interceptor)
        .build()

    @Provides
    fun provideRetrofit(okHttpClient: OkHttpClient, moshi: Moshi): Retrofit = Retrofit.Builder()
        .baseUrl(ConstantsApp.BASE_URL)
        .addCallAdapterFactory(LiveDataCallAdapterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(LiveDataResponseBodyConverterFactory.create())
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .client(okHttpClient)
        .build()

    @Provides
    fun provideMoshi(): Moshi = Moshi.Builder()
        .add(KotlinJsonAdapterFactory())
        .build()

    @Provides
    fun provideApiClient(retrofit: Retrofit): ApiInterface = retrofit.create(ApiInterface::class.java)

    @Provides
    fun provideInterceptor() = object: Interceptor{
        override fun intercept(chain: Interceptor.Chain): Response {
            val request = chain.request()

            val url = request.url
                .newBuilder()
                .build()

            val response = chain.proceed(
                request
                    .newBuilder()
                    .url(url)
                    .build()
            )
            return response
        }
    }

}