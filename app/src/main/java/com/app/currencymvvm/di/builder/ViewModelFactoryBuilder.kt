package com.app.currencymvvm.di.builder

import androidx.lifecycle.ViewModelProvider
import com.app.currencymvvm.view_model.ViewModelFactory
import dagger.Binds
import dagger.Module

@Module(includes = [(ViewModelsBuilder::class)])
abstract class ViewModelFactoryBuilder {

    @Binds
    abstract fun bindViewModelFactory(viewModelFactory: ViewModelFactory): ViewModelProvider.Factory

}