package com.app.currencymvvm.di.providers

import com.app.currencymvvm.ui.fragment.AlbumsFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class AlbumsFragmentProvider {

    @ContributesAndroidInjector
    abstract fun provideAlbumsFragment(): AlbumsFragment
}