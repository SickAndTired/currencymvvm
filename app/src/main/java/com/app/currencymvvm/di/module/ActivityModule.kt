package com.app.currencymvvm.di.module

import com.app.currencymvvm.di.providers.PhotosFragmentProvider
import com.app.currencymvvm.di.providers.AlbumsFragmentProvider
import com.app.currencymvvm.di.providers.PhotoDetailFragmentProvider
import com.app.currencymvvm.ui.activity.MainActivity
import dagger.Module
import dagger.android.AndroidInjectionModule
import dagger.android.ContributesAndroidInjector

@Module(includes = [AndroidInjectionModule::class])
interface ActivityModule {

    @ContributesAndroidInjector(
        modules = [
            AlbumsFragmentProvider::class,
            PhotosFragmentProvider::class,
            PhotoDetailFragmentProvider::class
        ])
    fun mainActivityInjector(): MainActivity

}