package com.app.currencymvvm.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.app.currencymvvm.model.db.Photo
import io.reactivex.Single

@Dao
interface PhotoDao {

    @Query("SELECT * FROM photos")
    fun getAll(): Single<List<Photo>>

    @Query("SELECT * FROM photos WHERE id =:id")
    fun getOneById(id: Long): Single<Photo>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(photo: Photo)
}