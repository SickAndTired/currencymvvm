package com.app.currencymvvm.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.app.currencymvvm.db.dao.AlbumDao
import com.app.currencymvvm.db.dao.PhotoDao
import com.app.currencymvvm.model.db.Album
import com.app.currencymvvm.model.db.Photo

@Database(
    entities = [
        Album::class,
        Photo::class
    ],
    version = 1
)
abstract class AppDatabase: RoomDatabase() {

    abstract fun albumDao(): AlbumDao
    abstract fun photoDao(): PhotoDao
}