package com.app.currencymvvm.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.app.currencymvvm.model.db.Album
import io.reactivex.Single

@Dao
interface AlbumDao {

    @Query("SELECT * FROM albums")
    fun getAll(): Single<List<Album>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(album: Album)
}