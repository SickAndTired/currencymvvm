package com.app.currencymvvm.model.db

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(
    tableName = "photos"
)
data class Photo (
    @PrimaryKey(autoGenerate = false)
    val id: Long,

    val albumId: Long,

    val title: String,

    val url: String,

    val thumbnailUrl: String
)