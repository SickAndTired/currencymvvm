package com.app.currencymvvm.model.nw

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class NwAlbum(
    @field:Json(name = "userId")
    var userId: Long,

    @field:Json(name = "id")
    var id: Long,

    @field:Json(name = "title")
    var title: String
)