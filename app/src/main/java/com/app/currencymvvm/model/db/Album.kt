package com.app.currencymvvm.model.db

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(
    tableName = "albums"
)
data class Album (
    @PrimaryKey(autoGenerate = false)
    val id: Long,

    val userId: Long,

    val title: String
)