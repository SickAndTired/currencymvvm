package com.app.currencymvvm.view_model

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.app.currencymvvm.api.ApiClient
import com.app.currencymvvm.db.AppDatabase
import com.app.currencymvvm.model.db.Album
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

class AlbumsViewModel @Inject constructor(
    private val apiClient: ApiClient,
    private val appDatabase: AppDatabase
): ViewModel() {

    val albumsReceivedLiveData = MutableLiveData<List<Album>>()
    val isLoad = MutableLiveData<Boolean>()
    val albumData = MutableLiveData<Album>()
    private val compositeDisposable =  CompositeDisposable()

    init {
        isLoad.value = false
    }

    val album: Album? get() = albumData.value

    fun set(album: Album) = run { albumData.value = album }

    fun loadAlbumsFromApi() {
        compositeDisposable.add(
            apiClient.getAlbums()
                .map { nwAlbumList -> for(nwAlbum in nwAlbumList) {
                    appDatabase.albumDao().insert(
                        Album(
                            id = nwAlbum.id,
                            title = nwAlbum.title,
                            userId = nwAlbum.userId
                        )
                    )
                } }
                .flatMap { appDatabase.albumDao().getAll() }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {
                        isLoad.value = true
                        albumsReceivedLiveData.value = it
                    },
                    {
                        Timber.e("$it")
                        it.printStackTrace()
                    }
                )
        )
    }

    fun loadAlbumsFromDb(){
        compositeDisposable.add(
            appDatabase.albumDao().getAll()
                .toFlowable()
                .take(1)
                .filter { it.isNotEmpty() }
                .switchIfEmpty { loadAlbumsFromApi() }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {
                        isLoad.value = true
                        albumsReceivedLiveData.value = it
                    },
                    {
                        Timber.e("$it")
                        it.printStackTrace()
                    }
                )
        )
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}