package com.app.currencymvvm.view_model

import androidx.lifecycle.MutableLiveData
import com.app.currencymvvm.model.db.Photo

class PhotoViewModel(val photo: Photo) {

    val photoData = MutableLiveData<Photo>()

    init {
        photoData.value = photo
    }

}