package com.app.currencymvvm.view_model

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.app.currencymvvm.api.ApiClient
import com.app.currencymvvm.db.AppDatabase
import com.app.currencymvvm.model.db.Photo
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

class PhotosViewModel @Inject constructor(
    private val apiClient: ApiClient,
    private val appDatabase: AppDatabase
): ViewModel() {

    val photosReceivedLiveData = MutableLiveData<List<Photo>>()
    val isLoad = MutableLiveData<Boolean>()

    private val compositeDisposable =  CompositeDisposable()

    init {
        isLoad.value = false
    }

    fun loadPhotosFromDb(albumId: Long){
        compositeDisposable.add(
            appDatabase.photoDao().getAll()
                .toFlowable()
                .take(1)
                .filter { it.isNotEmpty() }
                .switchIfEmpty { loadPhotosFromApi(albumId) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {
                        isLoad.value = true
                        photosReceivedLiveData.value = it
                    },
                    {
                        Timber.e("$it")
                        it.printStackTrace()
                    }
                )
        )
    }

    private fun loadPhotosFromApi(albumId: Long){
        compositeDisposable.add(
            apiClient.getPhotosByAlbumId(albumId)
                .map { nwPhotoList -> for(nwPhoto in nwPhotoList) {
                    appDatabase.photoDao().insert(
                        Photo(
                            id = nwPhoto.id,
                            title = nwPhoto.title,
                            albumId = nwPhoto.albumId,
                            url = nwPhoto.url,
                            thumbnailUrl = nwPhoto.thumbnailUrl
                        )
                    )
                } }
                .flatMap { appDatabase.photoDao().getAll() }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {
                        isLoad.value = true
                        photosReceivedLiveData.value = it
                    },
                    {
                        Timber.e("$it")
                        it.printStackTrace()
                    }
                )
        )
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

}