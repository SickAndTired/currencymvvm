package com.app.currencymvvm.view_model

import androidx.lifecycle.MutableLiveData
import com.app.currencymvvm.model.db.Album

class AlbumViewModel(val album: Album) {

    val albumData = MutableLiveData<Album>()

    init {
        albumData.value = album
    }
}