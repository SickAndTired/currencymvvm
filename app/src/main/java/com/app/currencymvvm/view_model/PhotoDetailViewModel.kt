package com.app.currencymvvm.view_model

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.app.currencymvvm.api.ApiClient
import com.app.currencymvvm.db.AppDatabase
import com.app.currencymvvm.model.db.Photo
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

class PhotoDetailViewModel @Inject constructor(
    private val apiClient: ApiClient,
    private val appDatabase: AppDatabase
): ViewModel() {

    val photoReceivedLiveData = MutableLiveData<Photo>()
    val isLoad = MutableLiveData<Boolean>()

    private val compositeDisposable =  CompositeDisposable()

    init {
        isLoad.value = false
    }

    fun loadPhotoFromDb(photoId: Long){
        compositeDisposable.add(
            appDatabase.photoDao().getOneById(photoId)
                .toFlowable()
                .switchIfEmpty { loadPhotoFromApi(photoId) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {
                            photo ->
                        photoReceivedLiveData.value = photo
                        isLoad.value = true

                    },
                    {
                            error ->
                        Timber.e(error)
                        error.printStackTrace()
                    }
                )
        )
    }

    private fun loadPhotoFromApi(photoId: Long){
        compositeDisposable.add(
            apiClient.getPhotoDetailById(photoId)
                .map { nwPhoto -> appDatabase.photoDao().insert(
                    Photo(
                        id = nwPhoto.id,
                        albumId = nwPhoto.albumId,
                        title = nwPhoto.title,
                        url = nwPhoto.url,
                        thumbnailUrl = nwPhoto.thumbnailUrl
                    )) }
                .flatMap { appDatabase.photoDao().getOneById(photoId) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {
                            photo ->
                        photoReceivedLiveData.value = photo
                        isLoad.value = true

                    },
                    {
                            error ->
                        Timber.e(error)
                        error.printStackTrace()
                    }
                )
        )
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

}