package com.app.currencymvvm

import android.app.Application
import com.app.currencymvvm.di.component.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import timber.log.Timber
import javax.inject.Inject
import kotlin.properties.Delegates

class App: Application(), HasAndroidInjector  {

    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>

    override fun androidInjector(): AndroidInjector<Any> = androidInjector

    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())

        DaggerAppComponent
            .builder()
            .application(this)
            .build()
            .inject(this)
    }

    companion object {
        var instance: App by Delegates.notNull()
    }
//    private fun initDi(): AppComponent =
//        DaggerAppComponent.builder()
//            .appModule(AppModule(this))
//            .networkModule(NetworkModule())
//            .storageModule(StorageModule(this))
//            .viewModelModule(ViewModelModule())
//            .build()
}